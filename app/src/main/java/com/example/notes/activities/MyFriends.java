package com.example.notes.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.notes.AsyncTasks.RemoveFriend;
import com.example.notes.R;
import com.example.notes.adapters.MyFriendsAdapter;
import com.example.notes.objects.UserLoginInfo;
import com.example.notes.utilities.DatabaseHelper;

import java.util.ArrayList;

public class MyFriends extends AppCompatActivity {

    //Declare variables
    private static final String TAG = "MyFriends";
    private ArrayList<String> userEmailList;
    private ListView myFriendEmailListView;
    private DatabaseHelper dbHelper;
    private UserLoginInfo uli = null;
    private String removedFriend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_friends);
        dbHelper = new DatabaseHelper(this);
        uli = dbHelper.getLoggedInfo();

//        userEmailList = new ArrayList<>();
//        new GetMyFriend(MyFriends.this, userEmailList, uli.getUserEmail(), dbHelper).execute();
        setTitle(R.string.my_friends);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showResult();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_friend:
                deleteFriend();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteFriend() {
        new RemoveFriend(this, uli.getUserEmail(), removedFriend).execute();
        dbHelper.removeFriend(removedFriend);
        showResult();
    }

    private void handleClick(int position) {
        removedFriend = ((String) myFriendEmailListView.getItemAtPosition(position));
        for (int i = myFriendEmailListView.getFirstVisiblePosition(); i <= myFriendEmailListView.getLastVisiblePosition(); i++) {
            View child = myFriendEmailListView.getChildAt(i - myFriendEmailListView.getFirstVisiblePosition());
            child.setBackgroundResource(i == position ? R.color.light_gray : R.color.gray);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        handleClick(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (v.getId() == R.id.my_friends_list) {
//            MenuInflater inflater =  getActivity().getMenuInflater();
            getMenuInflater().inflate(R.menu.my_friends_long_press, menu);
        }
    }

    //Show the user`s friends
    public void showResult(){
        myFriendEmailListView = (ListView) findViewById(R.id.my_friends_list);
        registerForContextMenu(myFriendEmailListView);
        userEmailList = dbHelper.getAllFriends();
        if(userEmailList == null) return;
        MyFriendsAdapter adapter = new MyFriendsAdapter(this, R.layout.my_friends_object, userEmailList);
        myFriendEmailListView.setAdapter(adapter);
    }
}
