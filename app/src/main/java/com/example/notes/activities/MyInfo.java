package com.example.notes.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notes.AsyncTasks.Logout;
import com.example.notes.R;
import com.example.notes.objects.UserLoginInfo;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.ExifUtil;
import com.example.notes.utilities.Utilities;

public class MyInfo extends AppCompatActivity {
    private static final String TAG = "MyInfo";

    private DatabaseHelper dbHelper;
    private UserLoginInfo uli;
    private ImageView userImage;
    private String imagePath;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DatabaseHelper(this);
        uli = dbHelper.getLoggedInfo();

        if(uli == null){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.message))
                    .setMessage(getString(R.string.login_warring))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(MyInfo.this, LoginActivity.class));
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setCancelable(false);
            dialog.show();
        }else {
            setContentView(R.layout.activity_my_info);
            setTitle(getString(R.string.my_info));
            setUserImageClickAble();
            ((TextView) findViewById(R.id.email)).setText(uli.getUserEmail());
            ((TextView) findViewById(R.id.password)).setText(uli.getPassWord());
            if(uli.getImageUrl() != null){
                Bitmap bitmap = Utilities.loadBitmapImage(this, uli.getUserEmail());
                if (bitmap == null) userImage.setImageResource(R.drawable.ic_launcher_web);
                else userImage.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
//        Utilities.printLog(TAG, "onCreateOptionsMenu is called ------------------------>");
        getMenuInflater().inflate(R.menu.my_info_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Utilities.printLog(TAG, "onOptionsItemSelected is called ------------------------>");
        switch (item.getItemId()){
            case R.id.menu_add_friend:
                searchFriend();
                break;
            case R.id.menu_view_my_friend:
                startActivity(new Intent(this, MyFriends.class));
                break;
            case R.id.menu_log_out:
                logOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUserImageClickAble(){
        userImage = (ImageView) findViewById(R.id.user_image);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View alertLayout = getLayoutInflater().inflate(R.layout.alert_the_way_to_choose_image, null);
                final AlertDialog.Builder dialog = new AlertDialog.Builder(MyInfo.this)
                        .setView(alertLayout)
                        .setCancelable(true);

                final AlertDialog dialog1 = dialog.create();
                ((Button) alertLayout.findViewById(R.id.button_camera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        camera();
                        dialog1.dismiss();
                    }
                });
                ((Button) alertLayout.findViewById(R.id.button_gallery)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        gallery();
                        dialog1.dismiss();
                    }
                });
                dialog1.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK) {
            imagePath = getRealPathFromURI(imageReturnedIntent.getData());
            showImage(imagePath);
        }
    }

    private boolean checkPermission(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.e(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            showImage(imagePath);
        }else{
            Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
        }
    }

    private void showImage(String path){
        if(checkPermission()) {
            try {
                bitmap = ExifUtil.rotateBitmap(imagePath, Utilities.scaleDown(BitmapFactory.decodeFile(path), 500, true));     //do not rotate the image
                userImage.setImageBitmap(bitmap);
                Utilities.saveToInternalStorage(this, bitmap, uli.getUserEmail());
                dbHelper.updateUserImageUrl(uli, uli.getUserEmail());
            }catch (Exception e){
                Toast.makeText(this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private void camera(){
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);//zero can be replaced with any action code
    }

    private void gallery(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
    }

    private String getRealPathFromURI(Uri contentUri) {
        // can post image
        String [] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void searchFriend(){
        Intent newIntent = new Intent(this, AddFriendSearchable.class);
        newIntent.putExtra(Utilities.USER_EMAIL, uli.getUserEmail());
        startActivity(newIntent);
    }

    private void logOut(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.message))
                .setMessage(getString(R.string.logout_warring))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(dbHelper.logoutUser(uli.getUserEmail())){
                            dbHelper.emptyAllTable();
                            new Logout(getApplicationContext(), uli.getUserEmail()).execute();
                            Toast.makeText(getApplication(), getString(R.string.logged_out_warring), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), null);
        dialog.show();
    }
}
