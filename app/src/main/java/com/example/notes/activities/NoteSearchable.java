package com.example.notes.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.notes.R;
import com.example.notes.adapters.SearchNoteAdapter;
import com.example.notes.objects.Note;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;

import java.util.ArrayList;

public class NoteSearchable extends AppCompatActivity {
    private static final String TAG = "NoteSearchable";
    private ListView noteListView;
    private SearchView searchView;
    private DatabaseHelper dbHelper;
    private String searchKeyWord = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_searchable);
        if(savedInstanceState != null){
            searchKeyWord = savedInstanceState.getString(Utilities.KEY_SEARCH);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if(searchKeyWord != null)  outState.putString(Utilities.KEY_SEARCH, searchKeyWord);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        //        Utilities.printLog(TAG, "onCreateOptionsMenu is called ------------------------>");
        getMenuInflater().inflate(R.menu.searchable, menu);
        dbHelper = new DatabaseHelper(this);
        createSearchView(menu);
        return true;
    }

    private void createSearchView(Menu menu){
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchActionBarItem = menu.findItem(R.id.main_search_action_bar);
        searchView = (SearchView) searchActionBarItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                handleSearch(s);
                return false;
            }
        });
        searchView.setIconified(false);
        if(searchKeyWord != null) searchView.setQuery(searchKeyWord, true);
    }

    private void handleSearch(String data) {
        searchKeyWord = data;
        noteListView = (ListView) findViewById(R.id.search_result_list_notes);

        ArrayList<Note> notes = dbHelper.searchNoteByContent(data);

        if(notes == null || notes.isEmpty()) return;

        SearchNoteAdapter adapter = new SearchNoteAdapter(this, R.layout.note_search_object, notes);
        adapter.setSearchString(data);
        noteListView.setAdapter(adapter);
        noteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Note note = ((Note) noteListView.getItemAtPosition(position));
                Intent viewNoteIntent = new Intent(getApplicationContext(), NoteEditor.class);
                viewNoteIntent.putExtra(Note.NOTE_TABLE_COLUMN_ID, note.getmId());
                startActivity(viewNoteIntent);
                finish();
            }
        });
    }
}
