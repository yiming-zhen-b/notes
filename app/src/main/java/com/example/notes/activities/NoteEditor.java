package com.example.notes.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notes.MainActivity;
import com.example.notes.R;
import com.example.notes.objects.Note;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NoteEditor extends AppCompatActivity {
    private static final String TAG = "NoteEditor";

    private EditText noteTitle;
    private EditText noteContent;

    private int noteId = -1;
    private Note savedNote = null;
    private DatabaseHelper dbHelper = null;
    private int MY_LOCATION_RC = 0;
    private FusedLocationProviderClient mFusedLocationClient;
    private ProgressDialog dialog = null;
    private boolean retrievingLocation = false;
    private String IS_RETRIEVEING_LOCATION = "retrievingLocation";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_editor);
        setTitle(getString(R.string.note_editor));
        Intent intent = getIntent();
        dbHelper = new DatabaseHelper(this);

        noteTitle = (EditText)findViewById(R.id.note_title);
        noteContent = (EditText)findViewById(R.id.note_content);

        noteId = intent.getIntExtra(Note.NOTE_TABLE_COLUMN_ID, -1);
        if(noteId != -1){
            savedNote = dbHelper.getNoteById(noteId);
            if(savedNote != null){
                noteTitle.setText(savedNote.getmTitle());
                noteContent.setText(savedNote.getmContent());
            }
        }

        String action = intent.getAction();
        String type = intent.getType();
        if(Intent.ACTION_SEND.equals(action) && type != null && "text/plain".equals(type)){
            String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            if (sharedText != null) {
                noteContent.setText(sharedText);
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.getting_location));
//        Utilities.printLog(TAG, "On Create---------------------------->");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(retrievingLocation){
            requestPermission(MY_LOCATION_RC);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(retrievingLocation){
            dialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_RETRIEVEING_LOCATION, retrievingLocation);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            retrievingLocation = savedInstanceState.getBoolean(IS_RETRIEVEING_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length == 2 && grantResults[0]== PackageManager.PERMISSION_GRANTED && grantResults[1]== PackageManager.PERMISSION_GRANTED){
            requestPermission(requestCode);
        }
        else {
            Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
        }
    }

    private void requestPermission(int requestCode){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            checkGPS();
            if(requestCode == MY_LOCATION_RC){
                LocationRequest mLocationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(Utilities.UPDATE_INTERVAL)
                        .setFastestInterval(Utilities.FASTEST_INTERVAL);
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            }
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
        }
    }
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                dialog.dismiss();


                //The last location in the list is the newest
                Location currentLocation = locationList.get(locationList.size() - 1);

                Date currentTime = Calendar.getInstance().getTime();
                DateFormat dateFormat = new SimpleDateFormat(Utilities.DATE_FORMAT);

                StringBuilder locationString = new StringBuilder();
                locationString.append("\n");
                locationString.append(dateFormat.format(currentTime));
                locationString.append(" Your Location:").append("\n");
                locationString.append("Lat: ");
                locationString.append(currentLocation.getLatitude());
                locationString.append(", Lon: ");
                locationString.append(currentLocation.getLongitude()).append("\n");

                if(!noteContent.hasFocus()){
                    noteContent.requestFocus();
                }
                int cursorPosition = noteContent.getText().toString().length();

                noteContent.getText().insert(cursorPosition, locationString.toString());
                noteContent.setSelection(noteContent.getText().toString().length());

                mFusedLocationClient.removeLocationUpdates(this);
                retrievingLocation = false;
            }
        }
    };


    public void checkGPS(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Toast.makeText(getApplicationContext(), getString(R.string.dps_disabled_alert), Toast.LENGTH_SHORT).show();
                }
            }).setCancelable(false);
            dialog.show();
        }else{
            createDialog();
        }
    }

    private void createDialog(){
        retrievingLocation = true;
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.edit_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_new_note:
                saveNote();
                break;
            case R.id.delete_new_note:
                deleteNode();
                break;
            case R.id.share_note:
                shareNote();
                break;
            case R.id.add_my_current_location:
                requestPermission(MY_LOCATION_RC);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void shareNote(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        StringBuilder text = new StringBuilder();
        text.append(getString(R.string.note_title_share) + noteTitle.getText().toString() + "\n")
            .append(getString(R.string.note_content_share) + noteContent.getText().toString());

        sendIntent.putExtra(Intent.EXTRA_TEXT, text.toString());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_note_to)));
    }

    private void deleteNode() {
        if(savedNote == null) {
            finish();
        }else{
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.delete))
                    .setMessage(getString(R.string.delete_warring_1) + noteTitle.getText().toString() + getString(R.string.delete_warring_2))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(dbHelper.deleteNote(savedNote)){
                                Toast.makeText(getApplicationContext(), noteTitle.getText().toString() + getString(R.string.deleted), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(), noteTitle.getText().toString() + getString(R.string.cannot_delete), Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), null)
                    .setCancelable(true);
            dialog.show();
        }
    }

    private void saveNote(){
        final Note note;
        if(noteTitle.getText().toString().trim().isEmpty() || noteContent.getText().toString().trim().isEmpty()){
            Toast.makeText(this, getString(R.string.insert_title_content_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        if(savedNote != null){
            checkStatus(dbHelper.updateNote(new Note(savedNote.getmId(), noteTitle.getText().toString(), noteContent.getText().toString())));
            return;
        }
        checkStatus(dbHelper.insertNewNote(new Note(noteTitle.getText().toString(), noteContent.getText().toString())));

    }

    private void checkStatus(boolean bl){
        if (bl) {
            Toast.makeText(getApplicationContext(), getString(R.string.note_saved), Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.cannot_save_note), Toast.LENGTH_SHORT).show();
        }
    }
}
