package com.example.notes.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.notes.AsyncTasks.DeleteBackupNotes;
import com.example.notes.AsyncTasks.GetNoteBackupsSummary;
import com.example.notes.AsyncTasks.StartRestoreNoteBackups;
import com.example.notes.R;
import com.example.notes.adapters.NoteBackupsAdapter;
import com.example.notes.objects.NoteBackups;
import com.example.notes.objects.UserLoginInfo;
import com.example.notes.utilities.DatabaseHelper;

import java.util.ArrayList;

public class RestoreNotes extends AppCompatActivity {
    private static final String TAG = "RestoreNotes";

    private static final String STATE_KEY = "NOTE_BACKUPS";
    private ListView listBackupNotes;
    private DatabaseHelper dbHelper;
    private ArrayList<NoteBackups> noteBackups = null;
    private NoteBackups currentNoteBackups;
    private UserLoginInfo uli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DatabaseHelper(this);
        uli = dbHelper.getLoggedInfo();
        if(uli == null){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.message))
                    .setMessage(getString(R.string.login_warring))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(RestoreNotes.this, LoginActivity.class));
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setCancelable(false);
            dialog.show();
        }else {
            setContentView(R.layout.activity_restore_notes);
            setTitle(getString(R.string.note_backups));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_KEY, noteBackups);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            noteBackups = savedInstanceState.getParcelableArrayList(STATE_KEY);
        }
    }

    @Override
    public void onResume() {
//        Utilities.printLog(TAG, "onResume is called ------------------------>");
        super.onResume();
        if(uli != null) {
            if (noteBackups == null || noteBackups.size() == 0) {
                noteBackups = new ArrayList<>();
                new GetNoteBackupsSummary(noteBackups, this, this).execute();
            } else {
//            Log.d(TAG, "onResume noteBackups is not NULL");
                loadBackupNotes();
            }
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_backup:
                deleteBackup();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteBackup() {
        noteBackups.remove(currentNoteBackups);
        new DeleteBackupNotes(this, currentNoteBackups).execute();
        loadBackupNotes();
    }

    private void handleClick(int position) {
        currentNoteBackups = ((NoteBackups) listBackupNotes.getItemAtPosition(position));
//        ca.setSelectedItem(currentCategory);
        for (int i = listBackupNotes.getFirstVisiblePosition(); i <= listBackupNotes.getLastVisiblePosition(); i++) {
            View child = listBackupNotes.getChildAt(i - listBackupNotes.getFirstVisiblePosition());
            child.setBackgroundResource(i == position ? R.color.light_gray : R.color.gray);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        handleClick(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (v.getId() == R.id.notes_backup_list) {
//            MenuInflater inflater =  getActivity().getMenuInflater();
            getMenuInflater().inflate(R.menu.backup_note_long_press, menu);
        }
    }

    public void loadBackupNotes() {
        listBackupNotes = (ListView) findViewById(R.id.notes_backup_list);
        listBackupNotes.setAdapter(null);
        registerForContextMenu(listBackupNotes);

        if (noteBackups == null || noteBackups.isEmpty()) return;

        NoteBackupsAdapter na = new NoteBackupsAdapter(this, R.layout.note_backups_object, noteBackups);
        listBackupNotes.setAdapter(na);
        listBackupNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleClick(position);
                final View alertLayout = getLayoutInflater().inflate(R.layout.alert_the_way_to_restore_notes, null);
                final AlertDialog.Builder dialog = new AlertDialog.Builder(RestoreNotes.this)
                        .setView(alertLayout)
                        .setCancelable(true);

                final AlertDialog dialog1 = dialog.create();
                ((Button) alertLayout.findViewById(R.id.button_merge)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new StartRestoreNoteBackups(RestoreNotes.this, currentNoteBackups, false).execute();
                    }
                });
                ((Button) alertLayout.findViewById(R.id.button_override)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new StartRestoreNoteBackups(RestoreNotes.this, currentNoteBackups, true).execute();
                    }
                });
                dialog1.show();
            }
        });
    }
}
