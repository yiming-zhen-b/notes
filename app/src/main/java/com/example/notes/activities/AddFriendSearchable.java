package com.example.notes.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.notes.AsyncTasks.SearchUser;
import com.example.notes.AsyncTasks.SendFriendRequest;
import com.example.notes.R;
import com.example.notes.adapters.SearchUserAdapter;
import com.example.notes.objects.NewFriends;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;

import java.util.ArrayList;

public class AddFriendSearchable extends AppCompatActivity {

    //Declare variables
    private static final String TAG = "AddFriendSearchable";
    private ArrayList<NewFriends> newFriendEmailList;
    private ListView newFriendEmailListView;
    private SearchView searchView;
    private DatabaseHelper dbHelper;
    private String searchKeyWord = null;
    private String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_searchable);
        //Initialise variables
        newFriendEmailList = new ArrayList<>();
        userEmail = getIntent().getStringExtra(Utilities.USER_EMAIL);
        dbHelper = new DatabaseHelper(this);
    }

    //save data before the screen rotate
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (searchKeyWord != null) outState.putString(Utilities.KEY_SEARCH, searchKeyWord);
    }

    //retrieve data after the screen rotate
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            searchKeyWord = savedInstanceState.getString(Utilities.KEY_SEARCH);
        }
    }

    //create the customised menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //        Utilities.printLog(TAG, "onCreateOptionsMenu is called ------------------------>");
        getMenuInflater().inflate(R.menu.searchable, menu);
        createSearchView(menu);
        return true;
    }
    //create the search
    private void createSearchView(Menu menu) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchActionBarItem = menu.findItem(R.id.main_search_action_bar);
        searchView = (SearchView) searchActionBarItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchKeyWord = s;
                newFriendEmailList = new ArrayList<>();
                //send query when user click the submit
                new SearchUser(AddFriendSearchable.this, newFriendEmailList, userEmail, s).execute();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) { return false; }
        });
        searchView.setIconified(false);
        //restore the search keyword
        if (searchKeyWord != null) searchView.setQuery(searchKeyWord, true);
    }

    //show search result
    public void showResult() {
        newFriendEmailListView = (ListView) findViewById(R.id.search_result_list_new_friend);

        if(newFriendEmailList == null || newFriendEmailList.isEmpty()) return;

        SearchUserAdapter adapter = new SearchUserAdapter(this, R.layout.add_user_search_object, newFriendEmailList);
        adapter.setSearchString(searchKeyWord);
        newFriendEmailListView.setAdapter(adapter);
        newFriendEmailListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewFriends friend = ((NewFriends) newFriendEmailListView.getItemAtPosition(position));
                if(friend.isFriendB()){
                    Toast.makeText(getApplicationContext(), friend.getEmail() + getString(R.string.already_friend), Toast.LENGTH_SHORT).show();
                }else {
                    new SendFriendRequest(getApplicationContext(), userEmail, friend.getEmail()).execute();
                    Toast.makeText(getApplicationContext(), getString(R.string.friend_request_send_to) + friend.getEmail(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
