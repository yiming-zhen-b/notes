package com.example.notes.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.notes.objects.Note;
import com.example.notes.objects.Notifications;
import com.example.notes.objects.UserLoginInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";
    private Context context;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "notes_db";

    //SignIn table
    private static final String LOGIN_TABLE_NAME = "loggedIn";
    private static final String LOGIN_TABLE_ACTIVE_COLUMN_NAME = "active";
    private static final String LOGIN_TABLE_USER_EMAIL_COLUMN_NAME = "user_email";
    private static final String LOGIN_TABLE_PASSWORD_COLUMN_NAME = "user_pass";
    private static final String LOGIN_TABLE_IMAGE_URL = "image_url";

    private static final String LOGIN_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LOGIN_TABLE_NAME + "("
                    + LOGIN_TABLE_ACTIVE_COLUMN_NAME + " BOOLEAN,"
                    + LOGIN_TABLE_USER_EMAIL_COLUMN_NAME + " TEXT,"
                    + LOGIN_TABLE_PASSWORD_COLUMN_NAME + " TEXT,"
                    + LOGIN_TABLE_IMAGE_URL + " TEXT"
                    + ")";
    //Backup Table
    private static final String BACKUP_TABLE_NAME = "backup_summary";
    private static final String BACKUP_TABLE_NUMBER_COLUMN_NAME = "number";
    private static final String BACKUP_TABLE_TIME_COLUMN_NAME = "date";

    private static final String BACKUP_TABLE =
            "CREATE TABLE IF NOT EXISTS " + BACKUP_TABLE_NAME + "("
                    + BACKUP_TABLE_NUMBER_COLUMN_NAME + " TEXT,"
                    + BACKUP_TABLE_TIME_COLUMN_NAME + " DATETIME"
                    + ")";

    //Notification table
    private static final String NOTI_TABLE_NAME = "notifications";
    private static final String NOTI_ID  = "id";
    private static final String NOTI_TYPE = "type";
    private static final String NOTI_FROM = "from_user";
    private static final String NOTI_DATA = "data";
    private static final String NOTI_EXECUTED = "executed";


    private static final String NOTI_TABLE =
            "CREATE TABLE IF NOT EXISTS " + NOTI_TABLE_NAME + "("
                    + NOTI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + NOTI_TYPE + " TEXT,"
                    + NOTI_FROM + " TEXT,"
                    + NOTI_DATA + " TEXT,"
                    + NOTI_EXECUTED + " TEXT"
                    + ")";

    private static final String FRIEND_TABLE_NAME = "friends";
    private static final String FRIEND_TABLE_COLUMN_EMAIL = "email";

    private static final String FRIEND_TABLE =
            "CREATE TABLE IF NOT EXISTS " + FRIEND_TABLE_NAME + "("
                    + FRIEND_TABLE_COLUMN_EMAIL + " TEXT"
                    + ")";

    private static final String [] TABLE_NAMES = new String [] {NOTI_TABLE_NAME, BACKUP_TABLE_NAME, Note.NOTE_TABLE_NAME, LOGIN_TABLE_NAME, BACKUP_TABLE_NAME, FRIEND_TABLE_NAME};


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Note.CREATE_NOTE_TABLE);
        sqLiteDatabase.execSQL(LOGIN_TABLE);
        sqLiteDatabase.execSQL(BACKUP_TABLE);
        sqLiteDatabase.execSQL(NOTI_TABLE);
        sqLiteDatabase.execSQL(FRIEND_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void insertFriends(ArrayList<String> userEmailList){
        emptyTable(FRIEND_TABLE_NAME);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = null;
        for(String email : userEmailList){
            values = new ContentValues();
            values.put(FRIEND_TABLE_COLUMN_EMAIL, email);
            db.insert(FRIEND_TABLE_NAME, null, values);
        }
        db.close();
    }

    public boolean removeFriend(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(FRIEND_TABLE_NAME, FRIEND_TABLE_COLUMN_EMAIL + " =?",
                new String[]{email});
        db.close();
        return result != 0;
    }

    public ArrayList<String> getAllFriends(){
        ArrayList<String> email = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + FRIEND_TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                email.add(cursor.getString(cursor.getColumnIndex(FRIEND_TABLE_COLUMN_EMAIL)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return email;
    }

    public String [] getAllFriendsAsArray(){
        ArrayList<String> email = getAllFriends();
        return email.toArray(new String[email.size()]);
    }

    public void emptyTable(String tableName){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ tableName);
        db.close();
    }

    public void emptyAllTable(){
        for(String table : TABLE_NAMES){
            emptyTable(table);
        }
    }

    public void addNewNotification(Map<String, String> data){
        SQLiteDatabase db = this.getWritableDatabase();
        String type = data.get(Utilities.NOTIFICATION_KEY_TYPE);

//        String selectQuery = "SELECT * FROM " + NOTI_TABLE_NAME + " WHERE " + NOTI_TYPE + "=? and " + NOTI_DATA + "=?";
//        Cursor cursor = db.rawQuery(selectQuery, new String[]{ type, data.get(Utilities.NOTIFICATION_KEY_FROM) });
//        Log.e(TAG, "cursor.getCount() " + cursor.getCount());
//        if(cursor.getCount() == 0){
        ContentValues values = new ContentValues();
        values.put(NOTI_TYPE, type);
        values.put(NOTI_FROM, data.get(Utilities.NOTIFICATION_KEY_FROM));
        values.put(NOTI_DATA, data.get(Utilities.NOTIFICATION_KEY_DATA));
        values.put(NOTI_EXECUTED, Utilities.KEY_FALSE);

        db.insert(NOTI_TABLE_NAME, null, values);
//        }
//        cursor.close();
        db.close();
    }

    public boolean updateNotification(Notifications noti){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTI_EXECUTED, Utilities.KEY_TRUE);
        int id = db.update(NOTI_TABLE_NAME, values, NOTI_ID + "=?", new String[]{String.valueOf(noti.getId())});
        db.close();
        return id > 0;
    }

    public ArrayList<Notifications> getAllNotifications(){
        ArrayList<Notifications> notifications = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + NOTI_TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                notifications.add(new Notifications(cursor.getInt(cursor.getColumnIndex(NOTI_ID)),
                                cursor.getString(cursor.getColumnIndex(NOTI_TYPE)),
                                cursor.getString(cursor.getColumnIndex(NOTI_FROM)),
                                cursor.getString(cursor.getColumnIndex(NOTI_DATA)),
                                cursor.getString(cursor.getColumnIndex(NOTI_EXECUTED))
                        ));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return notifications;
    }

    public boolean removeNotifications(Notifications noti){
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(NOTI_TABLE_NAME, NOTI_ID + " =?", new String[]{String.valueOf(noti.getId())});
        db.close();
        return result != 0;
    }

    public String addNewBackupSummary() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String currentTime = Utilities.getCurrentDataTime();
        values.put(BACKUP_TABLE_NUMBER_COLUMN_NAME, getNotesNumber());
        values.put(BACKUP_TABLE_TIME_COLUMN_NAME, currentTime);

        db.insert(BACKUP_TABLE_NAME, null, values);
        db.close();

        return currentTime;
    }

    public boolean logoutUser(String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(LOGIN_TABLE_NAME, LOGIN_TABLE_USER_EMAIL_COLUMN_NAME + " = ?", new String[]{email});
        db.close();
        return result != 0;
    }

    public boolean updateUserImageUrl(UserLoginInfo user, String url){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOGIN_TABLE_IMAGE_URL, url);
        int id = db.update(LOGIN_TABLE_NAME, values, LOGIN_TABLE_USER_EMAIL_COLUMN_NAME + "=?", new String[]{user.getUserEmail()});
        db.close();
        return id > 0;
    }

    public UserLoginInfo getLoggedInfo() {
        UserLoginInfo uli = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + LOGIN_TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(cursor.getColumnIndex(LOGIN_TABLE_ACTIVE_COLUMN_NAME)).equalsIgnoreCase("true")) {
                    uli = new UserLoginInfo(cursor.getString(cursor.getColumnIndex(LOGIN_TABLE_USER_EMAIL_COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndex(LOGIN_TABLE_PASSWORD_COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndex(LOGIN_TABLE_IMAGE_URL)));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return uli;
    }

    public void addNewLoginInfo(String email, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LOGIN_TABLE_ACTIVE_COLUMN_NAME, "false");
        db.update(LOGIN_TABLE_NAME, values, "", null);

        values = new ContentValues();

        values.put(LOGIN_TABLE_USER_EMAIL_COLUMN_NAME, email);
        values.put(LOGIN_TABLE_PASSWORD_COLUMN_NAME, password);
        values.put(LOGIN_TABLE_ACTIVE_COLUMN_NAME, "true");

        db.insert(LOGIN_TABLE_NAME, null, values);

        db.close();
    }

    public ArrayList<Note> searchNoteByContent(String st) {
        ArrayList<Note> notes = new ArrayList<>();
        if (st.isEmpty()) return notes;
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + Note.NOTE_TABLE_NAME + " WHERE "
                + Note.NOTE_TABLE_COLUMN_CONTENT + " LIKE '%" + st + "%' OR "
                + Note.NOTE_TABLE_COLUMN_DATE + " LIKE '%" + st + "%' OR "
                + Note.NOTE_TABLE_COLUMN_TITLE + " LIKE '%" + st + "%'"
                + " ORDER BY " + Note.NOTE_TABLE_COLUMN_DATE + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                notes.add(new Note(cursor.getInt(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_DATE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_CONTENT))));

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return notes;
    }

    private int getNotesNumber() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Note> notes = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Note.NOTE_TABLE_NAME + " ORDER BY " + Note.NOTE_TABLE_COLUMN_DATE + " DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        int number = cursor.getCount();
        cursor.close();
        db.close();
        return number;
    }

    public ArrayList<Note> getAllNotes() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Note> notes = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Note.NOTE_TABLE_NAME + " ORDER BY " + Note.NOTE_TABLE_COLUMN_DATE + " DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                notes.add(new Note(cursor.getInt(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_DATE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_CONTENT))));

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return notes;
    }

    public String getAllNotesAsJSON() {
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + Note.NOTE_TABLE_NAME + " ORDER BY " + Note.NOTE_TABLE_COLUMN_DATE + " DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);
        JSONArray resultSet = new JSONArray();
        int totalColumn = cursor.getColumnCount();
        try {
            if (cursor.moveToFirst()) {
                do {
                    JSONObject rowObject = new JSONObject();
                    for (int i = 0; i < totalColumn; i++) {
                        if (cursor.getColumnName(i) != null) {
                            if (cursor.getString(i) != null) {
                                Log.d(TAG, cursor.getString(i));
                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "");
                            }
                        }
                    }
                    resultSet.put(rowObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        cursor.close();
        db.close();
        return resultSet.toString();
    }

    public boolean deleteNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(Note.NOTE_TABLE_NAME, Note.NOTE_TABLE_COLUMN_ID + " = ?", new String[]{String.valueOf(note.getmId())});
        db.close();
        return result != 0;
    }

    public void restoreNoteFromServer(String st, boolean isOverride) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if (isOverride) db.execSQL("delete from " + Note.NOTE_TABLE_NAME);
            JSONArray array = new JSONArray(st);
            ContentValues values;
            for (int i = 0; i < array.length(); i++) {
                values = new ContentValues();
                values.put(Note.NOTE_TABLE_COLUMN_TITLE, array.getJSONObject(i).getString(Note.NOTE_TABLE_COLUMN_TITLE));
                values.put(Note.NOTE_TABLE_COLUMN_CONTENT, array.getJSONObject(i).getString(Note.NOTE_TABLE_COLUMN_CONTENT));
                values.put(Note.NOTE_TABLE_COLUMN_DATE, array.getJSONObject(i).getString(Note.NOTE_TABLE_COLUMN_DATE));
                db.insert(Note.NOTE_TABLE_NAME, null, values);
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean insertNewNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Note.NOTE_TABLE_COLUMN_TITLE, note.getmTitle());
        values.put(Note.NOTE_TABLE_COLUMN_CONTENT, note.getmContent());
        values.put(Note.NOTE_TABLE_COLUMN_DATE, Utilities.getCurrentDataTime());

        long id = db.insert(Note.NOTE_TABLE_NAME, null, values);
        db.close();

        return id > 0;
    }

    public boolean updateNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Note.NOTE_TABLE_COLUMN_TITLE, note.getmTitle());
        values.put(Note.NOTE_TABLE_COLUMN_CONTENT, note.getmContent());
        values.put(Note.NOTE_TABLE_COLUMN_DATE, Utilities.getCurrentDataTime());
        int id = db.update(Note.NOTE_TABLE_NAME, values, Note.NOTE_TABLE_COLUMN_ID + "=?", new String[]{String.valueOf(note.getmId())});
        db.close();
        return id > 0;
    }

    public Note getNoteById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Note note = null;

        String selectQuery = "SELECT * FROM " + Note.NOTE_TABLE_NAME + " WHERE " + Note.NOTE_TABLE_COLUMN_ID + "=?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(id)});

        if (cursor.moveToFirst()) {
            do {
                note = new Note(cursor.getInt(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_DATE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Note.NOTE_TABLE_COLUMN_CONTENT)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return note;
    }


//    public boolean updateCategoryImageURL(String category, String url){
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(Category.CATEGORY_TABLE_COLUMN_IMAGE_URL, url);
//
//        int id = db.update(Category.CATEGORY_TABLE_NAME, values, Category.CATEGORY_TABLE_COLUMN_NAME + "=?", new String[]{category});
//        db.close();
//
//        return id < 1 ? false : true;
//    }
//
//    public ArrayList<Category> getAllCategory(){
//        ArrayList<Category> categories = new ArrayList<>();
//        String selectQuery = "SELECT * FROM " + Category.CATEGORY_TABLE_NAME + " ORDER BY " + Category.CATEGORY_TABLE_COLUMN_DATE + " ASC";
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//                categories.add(new Category(cursor.getString(cursor.getColumnIndex(Category.CATEGORY_TABLE_COLUMN_NAME))));
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        db.close();
//        return categories;
//    }
//
//    public String [] getAllCategoryName(){
//        ArrayList<Category> list = getAllCategory();
//        String [] categories = new String[list.size()];
//        for(int i = 0; i < list.size(); i ++){
//            categories[i] = list.get(i).getName();
//        }
//        return categories;
//    }
//
//    public String [] getAllCategoryNameWithoutDefaultCategory(){
//        ArrayList<Category> list = getAllCategory();
//        ArrayList<String> name = new ArrayList<>();
//        for(int i = 0; i < list.size(); i ++){
//            if(!Arrays.asList(Utilities.DEFAULT_CATEGORY).contains(list.get(i).getName()))
//                name.add(list.get(i).getName());
//        }
//        return name.toArray(new String[name.size()]);
//    }
//
//    public boolean insertNewCategory(String category){
//        if(!dataExists(Category.CATEGORY_TABLE_NAME, Category.CATEGORY_TABLE_COLUMN_NAME, category)){
//            SQLiteDatabase db = this.getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put(Category.CATEGORY_TABLE_COLUMN_NAME, category);
//            long id = db.insert(Category.CATEGORY_TABLE_NAME, null, values);
//
//            db.close();
//            return id <= 0 ? false : true;
//        }
//        return false;
//    }
//
//    public boolean deleteCategory(String category){
//        SQLiteDatabase db = this.getWritableDatabase();
//        int result = db.delete(Category.CATEGORY_TABLE_NAME, Category.CATEGORY_TABLE_COLUMN_NAME + " = ?", new String[]{category});
//        db.close();
//        return result == 0 ? false : true;
//    }
//
//    public boolean emptyCategory(String category){
//        SQLiteDatabase db = this.getWritableDatabase();
//        int result = db.delete(Note.NOTE_TABLE_NAME, Note.NOTE_TABLE_COLUMN_CATEGORY + " = ?", new String[]{category});
//        db.close();
//        return result == 0 ? false : true;
//    }
//
//    public JSONArray getAllData() {
//        JSONArray array = new JSONArray();
//        SQLiteDatabase db = this.getWritableDatabase();
//        try {
//            for (String name : Utilities.TABLE_NAMES) {
//                String searchQuery = "SELECT  * FROM " + name;
//                Cursor cursor = db.rawQuery(searchQuery, null);
//                JSONArray table = new JSONArray();
//                cursor.moveToFirst();
//                while (cursor.isAfterLast() == false) {
//                    int totalColumn = cursor.getColumnCount();
//                    JSONObject rowObject = new JSONObject();
//                    for (int i = 0; i < totalColumn; i++) {
//                        if (cursor.getColumnName(i) != null) {
//                            try {
//                                if (cursor.getString(i) != null) {
//                                    rowObject.put(cursor.getColumnName(i), cursor.getString(i));
//                                } else {
//                                    rowObject.put(cursor.getColumnName(i), "");
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                    table.put(rowObject);
//                    cursor.moveToNext();
//                }
//                cursor.close();
//                array.put(new JSONObject().put(name, table));
//            }
//            db.close();
////            Log.d("TAG_NAME", array.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return array;
//    }

//    public void insertDefaultData(String result) {
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            JSONObject jObject = new JSONObject(result);
//            ContentValues values;
//
//            for (String tableName : Utilities.TABLE_NAMES) {
//                JSONArray array = jObject.getJSONArray(tableName);
//                JSONArray keys = array.getJSONObject(0).names();
//                for (int a = 0; a < array.length(); a++) {
//                    values = new ContentValues();
//                    for (int i = 0; i < keys.length(); ++i) {
//                        String k = keys.getString(i); // Here's your key
//                        values.put(k, array.getJSONObject(a).getString(k));
//                    }
//                    db.insert(tableName, null, values);
//                }
//            }
//            db.close();
//        } catch (JSONException e) {
//            Log.e("log_tag", "Error parsing data " + e.toString());
//        }
//    }

//    private boolean dataExists(String tableName, String columnName, String data) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        String selectQuery = "SELECT * FROM " + tableName + " WHERE " + columnName + "=?";
//        Cursor cursor = db.rawQuery(selectQuery, new String[]{data});
//        int count = cursor.getCount();
//        cursor.close();
//        db.close();
//        return count <= 0 ? false : true;
//    }
//
//    public boolean defaultExists() {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = null;
//        int count = 0;
//        for(String cate : Utilities.DEFAULT_CATEGORY){
//            cursor = db.rawQuery("SELECT * FROM " + Category.CATEGORY_TABLE_NAME + " WHERE " + Category.CATEGORY_TABLE_COLUMN_NAME + "=?", new String[]{cate});
//            count = cursor.getCount() == 0 ? count : count + 1;
//        }
//        cursor.close();
//        db.close();
//        if(count == 2) return true;
//        return false;
//    }
}
