package com.example.notes.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class NetworkUtilities {
    private static final String TAG = "NetworkUtilities";

    public static HttpURLConnection createConnection(Context context, String url){
        HttpURLConnection httpConn = null;
        try {
            if(!serverIsRunning(context)) return null;
            httpConn = (HttpURLConnection) new URL(url).openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setConnectTimeout(20000);
            httpConn.setReadTimeout(20000);
            httpConn.setDoInput(true);
            httpConn.connect();
        }catch (Exception e) {
//            e.printStackTrace();
        }
        return httpConn;
    }

    public static void addParameter(HttpURLConnection httpConn, String para){
        try {
            DataOutputStream dos = new DataOutputStream(httpConn.getOutputStream());
            dos.writeBytes(para);
            dos.flush();
            dos.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getResponse(HttpURLConnection httpConn){
        String result = null;
        try {
            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpConn.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                result = bufferedReader.readLine();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    private static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
//            Toast.makeText(context, context.getString(R.string.not_connect_network), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private static boolean serverIsRunning(final Context context) {
        if (!isOnline(context)) return false;
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL(Utilities.BASE_URL).openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            Log.d(TAG, "getResponseCode" + urlc.getResponseCode());
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
//            new Handler(Looper.getMainLooper()).post(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(context.getApplicationContext(), context.getString(R.string.server_down), Toast.LENGTH_SHORT);
//                }
//            });

//            context.runOnUiThread(new Runnable() {
//                public void run() {
//
//                    Toast.makeText(context, context.getString(R.string.server_down), Toast.LENGTH_SHORT).show();
//                }
//            });
//            e.printStackTrace();
//            Toast.makeText(context, context.getString(R.string.server_down), Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    public static String encodeString(String st){
        try {
            return URLEncoder.encode(st, "UTF-8");
        }catch (Exception e){
            return "";
        }
    }
}
