package com.example.notes.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utilities {
    //server url
    public static final String BASE_URL = "http://192.168.0.100/personal_website/website/android/php/";
//    public static final String BASE_URL = "http://www.ditto.site/android/php/";
//    public static final String BASE_URL = "https://dittozhen.000webhostapp.com/android/php/";

    public static final String REGISTER_URL = BASE_URL + "Register.php";
    public static final String SIGN_IN_URL = BASE_URL + "SignIn.php";
    public static final String LOG_OUT_URL = BASE_URL + "LogOut.php";
    public static final String BACKUP_URL = BASE_URL + "MakeBackup.php";
    public static final String GET_BACKUP_SUMMARY_URL = BASE_URL + "GetNotesBackupSummary.php";
    public static final String DELETE_BACKUP_URL = BASE_URL + "DeleteNoteBackups.php";
    public static final String GET_BACKUP_CONTENT_URL = BASE_URL + "GetNoteBackupsByTime.php";
    public static final String UPDATE_TOKEN_URL = BASE_URL + "UpdateToken.php";
    public static final String SEARCH_USER_URL = BASE_URL + "SearchUser.php";
    public static final String SEND_FRIEND_REQUEST_URL = BASE_URL + "SendFriendRequest.php";
    public static final String ACCEPT_FRIEND_REQUEST_URL = BASE_URL + "AcceptFriendRequest.php";
    public static final String GET_MY_FRIEND_URL = BASE_URL + "GetMyFriends.php";
    public static final String REMOVE_FRIEND_URL = BASE_URL + "RemoveFriend.php";
    public static final String SEND_FRIEND_NOTE_URL = BASE_URL + "SendFriendNote.php";



    public static final String IMAGE_EXTENTION = ".jpg";
    public static final String IMAGE_DIR = File.separator + "imageDir" + File.separator;

    //Compare Keys
    public static final String KEY_TRUE = "true";
    public static final String KEY_FALSE = "false";


    //user current location update interval
    public static final long UPDATE_INTERVAL =  1000;    // milliseconds
    public static final long FASTEST_INTERVAL = 500;     //milliseconds

    public static final String DATABASE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

    //intent keys
    public static final String KEY_SEARCH = "SEARCH_KEYWORD";

    //search highlight color
    public static final String HIGHLIGHT_COLOR = "#ffff00";

    //Server side parameter Strings
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_NOTES = "data";
    public static final String SEARCH_KEY_WORD = "key_word";
    public static final String RESULT_STATUS = "status";
    public static final String RESULT_MESSAGE = "message";
    public static final String RESULT_KEY_DATA = "data";
    public static final String FRIEND_REQUEST_FROM = "from_user";
    public static final String FRIEND_REQUEST_TO = "to_user";
    public static final String BACKUP_SUMMARY_KEY_NUM = "num";
    public static final String BACKUP_SUMMARY_KEY_TIME = "backup_time";

    //Notification Parameters
    public static final int NOTIFICATION_ID = 0;
    public static final String NOTIFICATION_KEY_TITLE = "title";
    public static final String NOTIFICATION_KEY_BODY = "body";
    public static final String NOTIFICATION_KEY_TYPE = "noti_type";
    public static final String NOTIFICATION_KEY_FROM = "noti_from";
    public static final String NOTIFICATION_KEY_DATA = "noti_data";
    public static final String NOTIFICATION_EXISTS = "has_notification";
    public static final String NOTIFICATION_EXISTS_YES = "yes";

    public static final String NOTIFICATION_TYPE_FRIEND_REQUEST = "friend_request";
    public static final String NOTIFICATION_TYPE_FRIEND_ACCEPT = "friend_accept";
    public static final String NOTIFICATION_TYPE_NEW_NOTE = "new_note";



    public static String getCurrentDataTime(){
        return new SimpleDateFormat(Utilities.DATABASE_DATE_FORMAT).format(Calendar.getInstance().getTime());
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min((float) maxImageSize / realImage.getWidth(), (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
        return newBitmap;
    }

    public static boolean saveToInternalStorage(Context context, Bitmap bitmapImage, String userName){
        File categoryImageDir = new File(context.getFilesDir().getAbsolutePath() + IMAGE_DIR);
        if(!categoryImageDir.exists()) categoryImageDir.mkdir();
        FileOutputStream fos = null;
        try {
            File file = new File( categoryImageDir, userName + IMAGE_EXTENTION);

            fos = new FileOutputStream(file);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Bitmap loadBitmapImage(Context context, String userName)
    {
        Bitmap bitmap = null;
        try {
            File file = new File( context.getFilesDir().getAbsolutePath() + IMAGE_DIR, userName + IMAGE_EXTENTION);
            if(file.exists()){
                return BitmapFactory.decodeStream(new FileInputStream(file));
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


}
