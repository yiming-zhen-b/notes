package com.example.notes.utilities;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;

public class HighlightColor {

    public static SpannableString buildBackgroundColorSpan(String text, String searchString, String color) {
        SpannableString spannableString = new SpannableString(text);
        int indexOf = text.toUpperCase().indexOf(searchString.toUpperCase());
        if(indexOf < 0) return spannableString;
        try {
            spannableString.setSpan(new BackgroundColorSpan(Color.parseColor(color)), indexOf, (indexOf + searchString.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {

        }
        return spannableString;
    }

}
