package com.example.notes.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.activities.RestoreNotes;
import com.example.notes.objects.NoteBackups;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetNoteBackupsSummary extends AsyncTask<URL, String, String> {
    private static final String TAG = "GetNoteBackupsSummary";

    private Context context;
    private ProgressDialog dialog;
    private RestoreNotes restoreNotes;
    private DatabaseHelper dbHelper;
    private ArrayList<NoteBackups> noteBackups;

    public GetNoteBackupsSummary(ArrayList<NoteBackups> noteBackups, Context context, RestoreNotes restoreNotes) {
        this.context = context;
        this.restoreNotes = restoreNotes;
        this.noteBackups = noteBackups;
        dialog = new ProgressDialog(context);
        dbHelper = new DatabaseHelper(context);
    }

    @Override
    protected String doInBackground(URL... urls) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.GET_BACKUP_SUMMARY_URL + "?");

        if (httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();
//        dbHelper.insertDefaultData(result);

        return result;
    }

    @Override
    protected void onPreExecute() {
        this.dialog.setMessage(context.getString(R.string.fetching));
        this.dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        this.dialog.dismiss();
        if(result == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                noteBackups.add(new NoteBackups(object.getString(Utilities.BACKUP_SUMMARY_KEY_TIME), object.getString(Utilities.BACKUP_SUMMARY_KEY_NUM)));
                restoreNotes.loadBackupNotes();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String buildQuery() {
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(dbHelper.getLoggedInfo().getUserEmail());
    }
}

