package com.example.notes.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;

public class Logout extends AsyncTask<String, String, String> {
    private static final String TAG = "Logout";

    private Context context;

    private String email;

    public Logout(Context context, String email){
        this.context = context;
        this.email = email;
    }
    @Override
    protected String doInBackground(String... st) {
        String result = null;
        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.LOG_OUT_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);

        httpConn.disconnect();

        return result;
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(String result) {
        if(result == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
        }
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(email);
    }
}

