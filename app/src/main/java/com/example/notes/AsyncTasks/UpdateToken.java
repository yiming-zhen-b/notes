package com.example.notes.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;

public class UpdateToken extends AsyncTask<Boolean, String, String> {
    private static final String TAG = "UpdateToken";
    private static final String TOKEN = "token";

    private Context context;
    private String token;
    private String email;

    public UpdateToken(Context context, String email, String token){
        this.context = context;
        this.token = token;
        this.email = email;
    }
    @Override
    protected String doInBackground(Boolean... pra) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.UPDATE_TOKEN_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();
        return result;
    }

    @Override
    protected void onPostExecute(String res) {
        if(res == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
        }
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(email) + "&" +
                NetworkUtilities.encodeString(TOKEN) + "=" + NetworkUtilities.encodeString(token);
    }
}

