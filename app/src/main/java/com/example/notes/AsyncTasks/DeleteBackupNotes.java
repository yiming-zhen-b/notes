package com.example.notes.AsyncTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.objects.NoteBackups;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;

public class DeleteBackupNotes extends AsyncTask<String, String, Boolean> {
    private static final String TAG = "DeleteBackupNotes";

    private DatabaseHelper dbHelper;
    private NoteBackups noteBackups;
    private Context context;

    public DeleteBackupNotes(Activity activity, NoteBackups noteBackups){
        this.noteBackups = noteBackups;
        context = activity.getApplicationContext();
        dbHelper = new DatabaseHelper(activity);
    }
    @Override
    protected Boolean doInBackground(String... string) {

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.DELETE_BACKUP_URL + "?");

        if(httpConn == null) return null;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        String result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();

        return true;
    }

    private String buildQuery(){
//        Log.d(TAG, "USEREMAIL" + dbHelper.getLoggedInfo().getUserEmail());
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(dbHelper.getLoggedInfo().getUserEmail()) + "&" +
                NetworkUtilities.encodeString(Utilities.BACKUP_SUMMARY_KEY_TIME) + "=" + NetworkUtilities.encodeString(noteBackups.getTime());
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(Boolean result) {
        if(result == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
        }
    }
}

