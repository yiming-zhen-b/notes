package com.example.notes.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.objects.Notifications;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.NetworkUtilities;
import com.example.notes.utilities.Utilities;

import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.util.ArrayList;

public class GetMyFriend extends AsyncTask<String, String, String> {
    private static final String TAG = "GetMyFriend";

    private Context context;
    private DatabaseHelper dbHelper;

    private String userEmail;
    private ArrayList<String> userEmailList;
    private Notifications noti;

    public GetMyFriend(Context context, String userEmail, DatabaseHelper dbHelper, Notifications noti){
        this.context = context;
        this.userEmailList = new ArrayList<>();
        this.userEmail = userEmail;
        this.dbHelper = dbHelper;
        this.noti = noti;
    }
    @Override
    protected String doInBackground(String... string) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.GET_MY_FRIEND_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();

        return result;
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(userEmail);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(String result) {
        if(result == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++) {
                userEmailList.add(array.getString(i));
            }
            dbHelper.insertFriends(userEmailList);
            dbHelper.updateNotification(noti);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

