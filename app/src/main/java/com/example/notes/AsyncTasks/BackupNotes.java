package com.example.notes.AsyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;
import java.net.URL;

public class BackupNotes extends AsyncTask<URL, String, Boolean> {
    private static final String TAG = "BackupNotes";

    private Context context;
    private Activity activity;
    private ProgressDialog dialog;
    private DatabaseHelper dbHelper;

    public BackupNotes(Activity activity){
        this.activity = activity;
        this.context = activity.getApplicationContext();
        dialog = new ProgressDialog(activity);
        dbHelper = new DatabaseHelper(activity);
    }
    @Override
    protected Boolean doInBackground(URL... urls) {

        HttpURLConnection httpConn = NetworkUtilities.createConnection(activity, Utilities.BACKUP_URL + "?");

        if(httpConn == null) return null;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        String result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();

        return true;
    }

    private String buildQuery(){
//        Log.d(TAG, "USEREMAIL" + dbHelper.getLoggedInfo().getUserEmail());
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(dbHelper.getLoggedInfo().getUserEmail()) + "&" +
                NetworkUtilities.encodeString(Utilities.USER_NOTES) + "=" + NetworkUtilities.encodeString(dbHelper.getAllNotesAsJSON());
    }

    @Override
    protected void onPreExecute() {
        this.dialog.setMessage(context.getString(R.string.uploading_data));
        this.dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(Boolean result) {
        this.dialog.dismiss();
        if(result == null) {
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, context.getString(R.string.data_uploaded), Toast.LENGTH_SHORT).show();
        }
    }
}

