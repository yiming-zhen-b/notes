package com.example.notes.AsyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.activities.MyInfo;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.NetworkUtilities;
import com.example.notes.utilities.Utilities;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.net.HttpURLConnection;

public class RegisterAndSignIn extends AsyncTask<Boolean, String, String> {
    private static final String TAG = "RegisterAndSignIn";

    private Activity mActivity;
    private Context context;
    private AutoCompleteTextView emailTextView;

    private String email;
    private String password;
    private DatabaseHelper dbHelper;
    private ProgressDialog dialog;
    private boolean isSignIn;

    public RegisterAndSignIn(Activity activity, AutoCompleteTextView emailTextView, String email, String password, boolean isSignIn){
        mActivity = activity;
        this.context = activity.getApplicationContext();
        this.email = email;
        this.password = password;
        this.emailTextView = emailTextView;
        this.isSignIn = isSignIn;
        dbHelper = new DatabaseHelper(activity);
        dialog = new ProgressDialog(activity);
    }
    @Override
    protected String doInBackground(Boolean... pra) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, isSignIn ? Utilities.SIGN_IN_URL : Utilities.REGISTER_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();
        return result;
    }

    @Override
    protected void onPreExecute() {
        dialog.setMessage(isSignIn ? context.getString(R.string.sign_in_message) : context.getString(R.string.registering_message));
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String res) {
        this.dialog.dismiss();
        if(res == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONObject result = new JSONObject(res);
            if (!result.getBoolean(Utilities.RESULT_STATUS)) {
                emailTextView.setError(result.getString(Utilities.RESULT_MESSAGE));
            } else {
                dbHelper.addNewLoginInfo(email, password);

                Intent intent = new Intent(mActivity, MyInfo.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.getApplicationContext().startActivity(intent);
                mActivity.finish();
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(mActivity, new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        new UpdateToken(context, email, newToken).execute();
//                        Log.e("newToken",newToken);
                    }
                });
                Toast.makeText(context, isSignIn ? context.getString(R.string.welcome_back_message) : context.getString(R.string.new_welcome_message), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCancelled() {
        this.dialog.dismiss();
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(email) + "&" +
                NetworkUtilities.encodeString(Utilities.USER_PASSWORD) + "=" + NetworkUtilities.encodeString(password);
    }
}

