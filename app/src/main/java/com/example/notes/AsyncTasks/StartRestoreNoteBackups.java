package com.example.notes.AsyncTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.objects.NoteBackups;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;

public class StartRestoreNoteBackups extends AsyncTask<String, String, String> {
    private static final String TAG = "StartRestoreNoteBackups";

    private Context context;
    private Activity activity;
    private DatabaseHelper dbHelper;
    private boolean isOverride;
    private NoteBackups noteBackups;

    public StartRestoreNoteBackups(Activity activity, NoteBackups noteBackups, boolean isOverride){
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.isOverride = isOverride;
        this.noteBackups = noteBackups;
        dbHelper = new DatabaseHelper(activity);
    }
    @Override
    protected String doInBackground(String... string) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.GET_BACKUP_CONTENT_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();

        return result;
    }

    private String buildQuery(){
//        Log.d(TAG, "USEREMAIL" + dbHelper.getLoggedInfo().getUserEmail());
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(dbHelper.getLoggedInfo().getUserEmail()) + "&" +
                NetworkUtilities.encodeString(Utilities.BACKUP_SUMMARY_KEY_TIME) + "=" + NetworkUtilities.encodeString(noteBackups.getTime());
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(String result) {
        if(result == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        dbHelper.restoreNoteFromServer(result, isOverride);
        activity.finish();
        Toast.makeText(context, context.getString(R.string.data_restored), Toast.LENGTH_SHORT).show();
    }
}

