package com.example.notes.AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.activities.AddFriendSearchable;
import com.example.notes.objects.NewFriends;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

public class SearchUser extends AsyncTask<String, String, String> {
    private static final String TAG = "SearchUser";
    private static final String RESULT_KEY_EMAIL = "email";
    private static final String RESULT_KEY_ADDITIONAL_INFO= "additionalInfo";

    private Context context;
    private ProgressDialog dialog;
    private DatabaseHelper dbHelper;
    private AddFriendSearchable addFriendSearchable;
    private String userEmail, keyWord;
    private ArrayList<NewFriends> userEmailList;

    public SearchUser(AddFriendSearchable addFriendSearchable, ArrayList<NewFriends> userEmailList, String userEmail, String keyWord){
        this.addFriendSearchable = addFriendSearchable;
        this.context = addFriendSearchable.getApplicationContext();
        this.userEmailList = userEmailList;
        this.userEmail = userEmail;
        this.keyWord = keyWord;
        dialog = new ProgressDialog(addFriendSearchable);
        dbHelper = new DatabaseHelper(addFriendSearchable);
    }
    @Override
    protected String doInBackground(String... string) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.SEARCH_USER_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();

        return result;
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.USER_EMAIL) + "=" + NetworkUtilities.encodeString(userEmail) + "&" +
                NetworkUtilities.encodeString(Utilities.SEARCH_KEY_WORD) + "=" + NetworkUtilities.encodeString(keyWord);
    }

    @Override
    protected void onPreExecute() {
        this.dialog.setMessage(context.getString(R.string.searching));
        this.dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(String... progress) {}

    @Override
    protected void onPostExecute(String result) {
        this.dialog.dismiss();
        if (result == null) {
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                userEmailList.add(new NewFriends(object.getString(RESULT_KEY_EMAIL), object.getString(RESULT_KEY_ADDITIONAL_INFO)));
            }
            addFriendSearchable.showResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

