package com.example.notes.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.utilities.Utilities;
import com.example.notes.utilities.NetworkUtilities;

import java.net.HttpURLConnection;

public class SendFriendRequest extends AsyncTask<Boolean, String, String> {
    private static final String TAG = "SendFriendRequest";

    private Context context;
    private String from_email, to_email;

    public SendFriendRequest(Context context, String from_email, String to_email){
        this.context = context;
        this.from_email = from_email;
        this.to_email = to_email;
    }
    @Override
    protected String doInBackground(Boolean... pra) {
        String result = null;

        HttpURLConnection httpConn = NetworkUtilities.createConnection(context, Utilities.SEND_FRIEND_REQUEST_URL + "?");

        if(httpConn == null) return result;

        NetworkUtilities.addParameter(httpConn, buildQuery());

        result = NetworkUtilities.getResponse(httpConn);

        Log.e(TAG, "RESULT: " + result);
        httpConn.disconnect();
        return result;
    }

    @Override
    protected void onPostExecute(String res) {
        if(res == null){
            Toast.makeText(context, context.getString(R.string.server_network_warring), Toast.LENGTH_SHORT).show();
        }
    }

    private String buildQuery(){
        return NetworkUtilities.encodeString(Utilities.FRIEND_REQUEST_FROM) + "=" + NetworkUtilities.encodeString(from_email) + "&" +
                NetworkUtilities.encodeString(Utilities.FRIEND_REQUEST_TO) + "=" + NetworkUtilities.encodeString(to_email);
    }
}

