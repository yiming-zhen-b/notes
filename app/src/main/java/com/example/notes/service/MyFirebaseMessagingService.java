package com.example.notes.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.example.notes.MainActivity;
import com.example.notes.R;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCMService";
    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

//            Log.d(TAG, "Message data payload: " + remoteMessage.getFrom());
            sendNotification(remoteMessage.getData().get(Utilities.NOTIFICATION_KEY_TITLE),
                    remoteMessage.getData().get(Utilities.NOTIFICATION_KEY_BODY),
                    remoteMessage.getData().get(Utilities.NOTIFICATION_KEY_TYPE),
                    remoteMessage.getData().get(Utilities.NOTIFICATION_KEY_FROM));

            new DatabaseHelper(this).addNewNotification(remoteMessage.getData());
            broadcaster.sendBroadcast(new Intent("MyData"));
        }
    }

    @Override
    public void onNewToken(String token) {
//        Log.d(TAG, "Refreshed token: " + token);
//        new UpdateToken(email, token).execute();
//        sendRegistrationToServer(token);
    }



    private void sendNotification(String title, String messageBody, String type, String fromUser) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Utilities.NOTIFICATION_EXISTS, Utilities.NOTIFICATION_EXISTS_YES);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = "1";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_web)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(Utilities.NOTIFICATION_ID /* ID of notification */, notificationBuilder.build());
    }
}
