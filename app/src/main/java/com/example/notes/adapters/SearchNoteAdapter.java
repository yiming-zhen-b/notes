package com.example.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.notes.R;
import com.example.notes.objects.Note;
import com.example.notes.utilities.HighlightColor;
import com.example.notes.utilities.Utilities;

import java.util.ArrayList;

public class SearchNoteAdapter extends ArrayAdapter<Note> {
    private int resource;
    private String mSearchString = null;

    public SearchNoteAdapter(Context context, int resource, ArrayList<Note> objects){
        super(context, resource, objects);
        this.resource = resource;
    }

    public void setSearchString(String data){
        mSearchString = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, null);
        }

        Note notes = getItem(position);
        if(notes != null){
//            TextView category = (TextView) convertView.findViewById(R.id.list_notes_category);
            TextView title = (TextView) convertView.findViewById(R.id.list_notes_title);
            TextView date = (TextView) convertView.findViewById(R.id.list_notes_date);
            TextView content = (TextView) convertView.findViewById(R.id.list_notes_content);

//            category.setText(notes.getmCategory());
            title.setText(HighlightColor.buildBackgroundColorSpan(notes.getmTitle(), mSearchString, Utilities.HIGHLIGHT_COLOR));
            date.setText(HighlightColor.buildBackgroundColorSpan(notes.getDataTimeFormated(getContext()), mSearchString, Utilities.HIGHLIGHT_COLOR));
            content.setText(HighlightColor.buildBackgroundColorSpan(notes.getmContent(), mSearchString, Utilities.HIGHLIGHT_COLOR));
        }
        return convertView;
    }
}
