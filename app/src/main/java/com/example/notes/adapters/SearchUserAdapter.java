package com.example.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.notes.R;
import com.example.notes.objects.NewFriends;
import com.example.notes.utilities.HighlightColor;
import com.example.notes.utilities.Utilities;

import java.util.ArrayList;

public class SearchUserAdapter extends ArrayAdapter<NewFriends> {
    private int resource;
    private String mSearchString = null;

    public SearchUserAdapter(Context context, int resource, ArrayList<NewFriends> objects){
        super(context, resource, objects);
        this.resource = resource;
    }

    public void setSearchString(String data){
        mSearchString = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, null);
        }

        NewFriends friend = getItem(position);
        if(friend != null){
            ((TextView) convertView.findViewById(R.id.user_email)).setText(HighlightColor.buildBackgroundColorSpan(friend.getEmail(), mSearchString, Utilities.HIGHLIGHT_COLOR));
            ((TextView) convertView.findViewById(R.id.is_friend)).setText(friend.getAdditionalInfo());
        }
        return convertView;
    }
}
