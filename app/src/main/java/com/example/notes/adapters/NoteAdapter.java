package com.example.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.notes.R;
import com.example.notes.objects.Note;

import java.util.ArrayList;

public class NoteAdapter extends ArrayAdapter<Note> {
    private int resource;

    public NoteAdapter(Context context, int resource, ArrayList<Note> objects){
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, null);
        }

        Note notes = getItem(position);
        if(notes != null){
            TextView title = (TextView) convertView.findViewById(R.id.list_notes_title);
            TextView date = (TextView) convertView.findViewById(R.id.list_notes_date);
            TextView content = (TextView) convertView.findViewById(R.id.list_notes_content);

            title.setText(notes.getmTitle().length() > 10 ? notes.getmTitle().substring(0 , 10) : notes.getmTitle());
            date.setText(notes.getDataTimeFormated(getContext()));
            content.setText(notes.getmContent().length() > 30 ? notes.getmContent().substring(0, 30) : notes.getmContent());
        }

        return convertView;
    }
}
