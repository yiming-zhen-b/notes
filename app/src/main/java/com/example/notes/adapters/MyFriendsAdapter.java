package com.example.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.notes.R;

import java.util.ArrayList;

public class MyFriendsAdapter extends ArrayAdapter<String> {
    private int resource;
    private String mSearchString = null;

    public MyFriendsAdapter(Context context, int resource, ArrayList<String> objects){
        super(context, resource, objects);
        this.resource = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, null);
        }

        String email = getItem(position);
        if(email != null){
            TextView emailText = (TextView) convertView.findViewById(R.id.user_email);
            emailText.setText(email);
        }
        return convertView;
    }
}
