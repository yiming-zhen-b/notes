package com.example.notes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.notes.R;
import com.example.notes.objects.NoteBackups;

import java.util.ArrayList;

public class NoteBackupsAdapter extends ArrayAdapter<NoteBackups> {
    private int resource;
    private String notes;

    public NoteBackupsAdapter(Context context, int resource, ArrayList<NoteBackups> objects){
        super(context, resource, objects);
        notes = context.getString(R.string.note_backup_adapter_notes);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, null);
        }

        NoteBackups noteBackups = getItem(position);
        if(noteBackups != null){
            TextView num = (TextView) convertView.findViewById(R.id.notes_backups_number);
            TextView time = (TextView) convertView.findViewById(R.id.notes_backups_time);

            num.setText(noteBackups.getNumNotes() + notes);
            time.setText(noteBackups.getTime());
        }

        return convertView;
    }
}
