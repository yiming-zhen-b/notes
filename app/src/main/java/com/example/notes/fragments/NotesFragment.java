package com.example.notes.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.notes.AsyncTasks.SendFriendNote;
import com.example.notes.R;
import com.example.notes.activities.NoteEditor;
import com.example.notes.adapters.NoteAdapter;
import com.example.notes.objects.Note;
import com.example.notes.objects.UserLoginInfo;
import com.example.notes.utilities.DatabaseHelper;

import java.util.ArrayList;

public class NotesFragment extends Fragment {
    private static final String TAG = "NotesFragment";
    private View view;
    private ListView listViewNotes;
    private UserLoginInfo uli = null;
    private DatabaseHelper dbHelper;
    private Note currentNote;

    public NotesFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
//        Utilities.printLog(TAG, "onSaveInstanceState is called ------------------------>");
        super.onCreate(savedInstanceState);
        dbHelper = new DatabaseHelper(getContext());
        uli = dbHelper.getLoggedInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        Utilities.printLog(TAG, "onCreateView is called ------------------------>");
        view = inflater.inflate(R.layout.notes_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
//        Utilities.printLog(TAG, "onResume is called ------------------------>");
        super.onResume();
        uli = dbHelper.getLoggedInfo();
        loadNotes();
    }


    private void handleClick(int position) {
        currentNote = ((Note) listViewNotes.getItemAtPosition(position));
//        ca.setSelectedItem(currentCategory);
        for (int i = listViewNotes.getFirstVisiblePosition(); i <= listViewNotes.getLastVisiblePosition(); i++) {
            View child = listViewNotes.getChildAt(i - listViewNotes.getFirstVisiblePosition());
            child.setBackgroundResource(i == position ? R.color.light_gray : R.color.gray);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_note:
                deleteNote();
                return true;
            case R.id.share_note:
                shareNoteToFriend();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        handleClick(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (v.getId() == R.id.list_notes) {
//            MenuInflater inflater =  getActivity().getMenuInflater();
            getActivity().getMenuInflater().inflate(R.menu.note_long_press, menu);
        }
    }

    private void deleteNote() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.delete))
                .setMessage(getString(R.string.delete_warring_1) + currentNote.getmTitle() + getString(R.string.delete_warring_2))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dbHelper.deleteNote(currentNote)) {
                            Toast.makeText(getContext(), currentNote.getmTitle() + getString(R.string.deleted), Toast.LENGTH_SHORT).show();
                            onResume();
                        } else {
                            Toast.makeText(getContext(), currentNote.getmTitle() +getString(R.string.cannot_delete), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .setCancelable(true);
        dialog.show();
    }

    private void shareNoteToFriend(){
        final String[] friends = dbHelper.getAllFriendsAsArray();
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.select_friend))
                .setItems(friends, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new SendFriendNote(getContext(), uli.getUserEmail(), friends[which], currentNote.toJSONString()).execute();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null)
                .setCancelable(true);
        dialog.show();
    }

    public void loadNotes() {
        listViewNotes = (ListView) view.findViewById(R.id.list_notes);
        listViewNotes.setAdapter(null);
        registerForContextMenu(listViewNotes);
        ArrayList<Note> notes = dbHelper.getAllNotes();

        if (notes == null || notes.size() == 0) return;

        NoteAdapter na = new NoteAdapter(getContext(), R.layout.note_object, notes);
        listViewNotes.setAdapter(na);
        listViewNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleClick(position);
                int noteId = ((Note) listViewNotes.getItemAtPosition(position)).getmId();
                Intent viewNoteIntent = new Intent(getContext(), NoteEditor.class);
                viewNoteIntent.putExtra(Note.NOTE_TABLE_COLUMN_ID, noteId);
                startActivity(viewNoteIntent);
            }
        });
    }
}
