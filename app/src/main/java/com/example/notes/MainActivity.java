package com.example.notes;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.notes.AsyncTasks.AcceptFriendRequest;
import com.example.notes.AsyncTasks.BackupNotes;
import com.example.notes.AsyncTasks.GetMyFriend;
import com.example.notes.activities.LoginActivity;
import com.example.notes.activities.MyInfo;
import com.example.notes.activities.NoteEditor;
import com.example.notes.activities.NoteSearchable;
import com.example.notes.activities.RestoreNotes;
import com.example.notes.fragments.NotesFragment;
import com.example.notes.objects.Note;
import com.example.notes.objects.Notifications;
import com.example.notes.objects.UserLoginInfo;
import com.example.notes.utilities.DatabaseHelper;
import com.example.notes.utilities.Utilities;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{
    private static final String TAG = "MainActivity";

    private DatabaseHelper dbHelper;
    private UserLoginInfo uli = null;
    private ArrayList<Notifications> notifications;
    private Button notifCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Utilities.printLog(TAG, "onCreate is called ------------------------>");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DatabaseHelper(this);
        uli = dbHelper.getLoggedInfo();
        notifications = dbHelper.getAllNotifications();

        String hasNoti = getIntent().getStringExtra(Utilities.NOTIFICATION_EXISTS);
        if (hasNoti != null && hasNoti.equalsIgnoreCase(Utilities.NOTIFICATION_EXISTS_YES)) {
            checkNotification();
        }
        addMyInfoClickListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        Utilities.printLog(TAG, "onCreateOptionsMenu is called ------------------------>");
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        handleNotificationMenuIcon(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Utilities.printLog(TAG, "onOptionsItemSelected is called ------------------------>");

        switch (item.getItemId()) {
            case R.id.main_menu_create_new_note:
                switchToNoteEditor();
                break;
            case R.id.main_search_menu_item:
                startActivity(new Intent(this, NoteSearchable.class));
                break;
            case R.id.main_menu_backup_note:
                backupNotes();
                break;
            case R.id.main_menu_restore_note:
                restoreNotes();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handleNotificationMenuIcon(Menu menu){
        MenuItem item = menu.findItem(R.id.notification);
        item.setActionView(R.layout.notification_count);
        notifCount = (Button) item.getActionView();
        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNotification();
            }
        });
    }

    private void backupNotes() {
        if (dbHelper.getLoggedInfo() == null) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.message))
                    .setMessage(getString(R.string.login_warring))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        }
                    })
                    .setNegativeButton(getString(R.string.no), null)
                    .setCancelable(false);
            dialog.show();
        } else {
            new BackupNotes(this).execute();
        }
    }

    private void checkNotification(){
        notifications = dbHelper.getAllNotifications();
        if(notifications.size() > 0) {
            final Notifications noti = notifications.get(0);
            if (noti.getType().equalsIgnoreCase(Utilities.NOTIFICATION_TYPE_FRIEND_REQUEST)) {
                final View alertLayout = getLayoutInflater().inflate(R.layout.notice_user_friend_request, null);
                ((TextView) alertLayout.findViewById(R.id.friend_request_message)).setText(noti.getFrom() + getString(R.string.friend_request_message));
                final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this)
                        .setView(alertLayout)
                        .setCancelable(true);

                final AlertDialog dialog1 = dialog.create();
                ((Button) alertLayout.findViewById(R.id.button_yes)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AcceptFriendRequest(view.getContext(), uli.getUserEmail(), noti.getFrom()).execute();
                        dbHelper.removeNotifications(noti);
                        showNotificationCount();
                        removeNotification();
                        dialog1.dismiss();
                    }
                });
                ((Button) alertLayout.findViewById(R.id.button_no)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showNotificationCount();
                        dialog1.dismiss();
                    }
                });
                dialog1.show();
            }
            if(noti.getType().equalsIgnoreCase(Utilities.NOTIFICATION_TYPE_FRIEND_ACCEPT)){
                AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.message))
                    .setMessage(getString(R.string.friend_accept_notice) + noti.getFrom())
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dbHelper.removeNotifications(noti);
                            showNotificationCount();
                            removeNotification();
                        }
                    })
                    .setCancelable(false);
                dialog.show();
            }
            if(noti.getType().equalsIgnoreCase(Utilities.NOTIFICATION_TYPE_NEW_NOTE)){
                AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.message))
                        .setMessage(getString(R.string.new_note_notice) + noti.getFrom())
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dbHelper.insertNewNote(Note.stringToNote(noti.getData()));
                                dbHelper.removeNotifications(noti);
                                showNotificationCount();
                                removeNotification();
                                NotesFragment fragment = (NotesFragment)getSupportFragmentManager().findFragmentById(R.id.notes);
                                fragment.loadNotes();
                            }
                        })
                        .setCancelable(false);
                dialog.show();
            }
        }
    }

    private void showNotificationCount(){
        notifications = dbHelper.getAllNotifications();
        if(notifCount != null) {
            notifCount.setText(String.valueOf(notifications.size()));
        }
        for(Notifications ni : notifications){
            if(ni.getType().equalsIgnoreCase(Utilities.NOTIFICATION_TYPE_FRIEND_ACCEPT) && !ni.isExecuted()){
                new GetMyFriend(this, uli.getUserEmail(), dbHelper, ni).execute();
            }
        }
    }

    private void removeNotification(){
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Utilities.NOTIFICATION_ID);
    }

    private void restoreNotes() {
        startActivity(new Intent(this, RestoreNotes.class));
    }

    private void switchToNoteEditor() {
        Intent intent = new Intent(this, NoteEditor.class);
        startActivity(intent);
    }

    private void addMyInfoClickListener() {
        FloatingActionButton myInfo = (FloatingActionButton) findViewById(R.id.my_info);
        myInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), MyInfo.class));
            }
        });
    }

    @Override
    public void onResume() {
//        Utilities.printLog(TAG, currentCategory + "  onResume is called ------------------------>");
        super.onResume();
        showNotificationCount();
        uli = dbHelper.getLoggedInfo();
    }

    @Override
    protected void onStop() {
//        Utilities.printLog(TAG, "  onStop is called ------------------------>");
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);

    }

    @Override
    protected void onDestroy() {
//        Utilities.printLog(TAG, "  onDestroy is called ------------------------>");
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver), new IntentFilter("MyData"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showNotificationCount();
        }
    };
}
