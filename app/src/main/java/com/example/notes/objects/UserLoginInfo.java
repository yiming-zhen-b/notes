package com.example.notes.objects;

public class UserLoginInfo {

    private String userEmail, passWord, imageUrl;


    public UserLoginInfo(String userEmail, String passWord, String imageUrl) {
        this.userEmail = userEmail;
        this.passWord = passWord;
        this.imageUrl = imageUrl;

    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUserEmail() {

        return userEmail;
    }

    public String getPassWord() {
        return passWord;
    }
}
