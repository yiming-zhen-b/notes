package com.example.notes.objects;

import android.content.Context;

import com.example.notes.utilities.Utilities;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Note implements Serializable{
    private int mId;
    private String mDataTime;
    private String mTitle;
    private String mContent;

    public static final String NOTE_TABLE_NAME = "notes";
    public static final String NOTE_TABLE_COLUMN_ID = "id";
    public static final String NOTE_TABLE_COLUMN_TITLE = "title";
    public static final String NOTE_TABLE_COLUMN_CONTENT = "content";
    public static final String NOTE_TABLE_COLUMN_DATE = "date";

    // Create table SQL query
    public static final String CREATE_NOTE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + NOTE_TABLE_NAME + "("
                    + NOTE_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + NOTE_TABLE_COLUMN_TITLE + " TEXT,"
                    + NOTE_TABLE_COLUMN_CONTENT + " TEXT,"
                    + NOTE_TABLE_COLUMN_DATE + " DATETIME"
                    + ")";

    //constructor for the search note from database
    public Note(int id, String date, String title, String content){
        mId = id;
        mDataTime = date;
        mTitle = title;
        mContent = content;
    }

    //constructor for the reedited note
    public Note(int id, String title, String content){
        mId = id;
        mTitle = title;
        mContent = content;
    }

    //constructor for new note
    public Note(String title, String content){
        mTitle = title;
        mContent = content;
    }

    public String toJSONString(){
        try{
            JSONObject object = new JSONObject();
            object.put(NOTE_TABLE_COLUMN_TITLE, mTitle);
            object.put(NOTE_TABLE_COLUMN_CONTENT, mContent);
            return object.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static Note stringToNote(String string){
        try{
            JSONObject object = new JSONObject(string);
            return new Note(object.getString(NOTE_TABLE_COLUMN_TITLE), object.getString(NOTE_TABLE_COLUMN_CONTENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        try{

        }catch (Exception e){

        }
        return NOTE_TABLE_COLUMN_TITLE + ": " + mTitle + "\n" + NOTE_TABLE_COLUMN_CONTENT + ": " + mContent;
    }

    public int getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmContent() {
        return mContent;
    }

    public String getDataTimeFormated(Context context){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Utilities.DATABASE_DATE_FORMAT);
            Date d = sdf.parse(mDataTime);
            sdf = new SimpleDateFormat(Utilities.DATE_FORMAT, context.getResources().getConfiguration().locale);
            sdf.setTimeZone(TimeZone.getDefault());
            return sdf.format(d);
        }catch (Exception e){
            e.printStackTrace();
            return "00/00/0000 00:00:00";
        }
    }
}
