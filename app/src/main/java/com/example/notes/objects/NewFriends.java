package com.example.notes.objects;

public class NewFriends {
    private static final String FRIEND = "friend";
    private String email;
    private String additionalInfo;

    public NewFriends(String email, String add) {
        this.email = email;
        this.additionalInfo = add;
    }

    public String getEmail() {
        return email;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public boolean isFriendB(){
        return additionalInfo.equalsIgnoreCase(FRIEND);
    }
}
