package com.example.notes.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class NoteBackups implements Parcelable{

    private String time;
    private String numNotes;

    public NoteBackups(String time, String numNotes) {
        this.time = time;
        this.numNotes = numNotes;
    }

    public NoteBackups(Parcel in) {
        this.time = in.readString();
        this.numNotes = in.readString();
    }

    public String getTime() {
        return time;
    }

    public String getNumNotes() {
        return numNotes;
    }

    public void setTime(String time) {

        this.time = time;
    }
    public static final Parcelable.Creator<NoteBackups> CREATOR = new Parcelable.Creator<NoteBackups>() {
        public NoteBackups createFromParcel(Parcel in) {
            return new NoteBackups(in);
        }

        public NoteBackups[] newArray(int size) {
            return new NoteBackups[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(time);
        parcel.writeString(numNotes);
    }

    public void setNumNotes(String numNotes) {
        this.numNotes = numNotes;
    }

    @Override
    public int hashCode() {
        return time.hashCode() ^ numNotes.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        NoteBackups nb = (NoteBackups)obj;
        return nb.time.equals(this.time) && nb.numNotes.equals(this.numNotes);
    }
}
