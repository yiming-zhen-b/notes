package com.example.notes.objects;

import com.example.notes.utilities.Utilities;

public class Notifications {
    private int id;
    private String type;
    private String from;
    private String data;
    private boolean executed;

    public Notifications(int id, String type, String from, String data, String executed) {
        this.id = id;
        this.type = type;
        this.from = from;
        this.data = data;
        this.executed = executed.equalsIgnoreCase(Utilities.KEY_TRUE);

    }
    public boolean isExecuted() {
        return executed;
    }
    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public String getFrom() {
        return from;
    }
    public String getData() {
        return data;
    }

}
